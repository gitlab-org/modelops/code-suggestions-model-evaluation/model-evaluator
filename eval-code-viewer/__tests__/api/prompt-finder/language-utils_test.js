const { languageIds } = require('__app__/constants');
const { commentLine } = require('__app__/api/prompt-finder/language-utils');

describe('languageUtils', () => {
  describe('commentLine', () => {
    const uncommentedLine = 'This line should be commented';
    it.each`
      languageId                | expected
      ${languageIds.javascript} | ${'// ' + uncommentedLine}
      ${languageIds.python}     | ${'# ' + uncommentedLine}
      ${languageIds.golang}     | ${'// ' + uncommentedLine}
      ${languageIds.c}          | ${'// ' + uncommentedLine}
    `(
      'for $languageId it should output "$expected"',
      ({ languageId, expected }) => {
        const result = commentLine(uncommentedLine, languageId);
        expect(result).toBe(expected);
      }
    );
  });
});
