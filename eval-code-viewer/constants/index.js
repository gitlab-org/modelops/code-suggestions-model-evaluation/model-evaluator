const languageModels = [
  'anthropic',
  'anthropic_fast',
  'vertex_combined',
  'vertex_text_bison',
  'vertex_code_bison',
  'vertex_gecko',
  'gitlab_model',
];

const languageIds = {
  javascript: 'javascript',
  python: 'python',
  golang: 'golang',
  c: 'c',
};

const programmingLanguages = [
  { id: languageIds.javascript, name: 'JS', highlightId: 'javascript' },
  { id: languageIds.python, name: 'python', highlightId: 'python' },
  { id: languageIds.golang, name: 'go', highlightId: 'go' },
  { id: languageIds.c, name: 'c', highlightId: 'clike' },
];

const isSupportedProgrammingLanguage = (languageId) =>
  programmingLanguages.map((language) => language.id).includes(languageId);

module.exports = {
  languageModels,
  languageIds,
  programmingLanguages,
  isSupportedProgrammingLanguage,
};
