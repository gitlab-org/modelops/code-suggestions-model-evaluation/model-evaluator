const fs = require('fs');
const { env } = require('process');
const bodyParser = require('body-parser');
const app = require('express')();
const { isSupportedProgrammingLanguage } = require('../constants');
const Vote = require('../models/vote');
const PromptFinder = require('./prompt-finder');
const { useAuth, authRoutes, authenticated } = require('./auth');

const rawData = fs.readFileSync('../eval-code-completion/test-results.json');
const testResults = JSON.parse(rawData);

const promptsVersion = env.PROMPT_VERSION;

const promptFinder = new PromptFinder('../results', promptsVersion);

app.use(bodyParser.json());

useAuth(app);

app.use(authRoutes);

app.all('/results', (req, res) => {
  return res.json(testResults);
});

app.get('/prompts/:languageId', authenticated, (req, res) => {
  const { languageId } = req.params;
  if (!isSupportedProgrammingLanguage(languageId)) {
    return res.status(400).json({ error: 'Invalid parameter: languageId' });
  }

  try {
    const promptsData = promptFinder.getPromptsForLanguage(languageId);
    return res.json({ promptsVersion, promptsData });
  } catch (error) {
    return res.sendStatus(500);
  }
});

app.post('/vote/:promptResultId', authenticated, async (req, res) => {
  const vote = new Vote(req.params.promptResultId, req.query);

  try {
    await vote.save();

    if (vote.validationError) {
      return res.status(400).json({ error: vote.validationError });
    }

    res.status(200).json({ message: 'Vote recorded successfully' });
  } catch (e) {
    console.log(e);
    res
      .status(500)
      .json({ error: 'An error occurred while recording the vote' });
  }
});

module.exports = app;
