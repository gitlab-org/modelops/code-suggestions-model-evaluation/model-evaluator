const { languageIds } = require('../../constants');

const commentLineMapping = {
  [languageIds.javascript]: '//',
  [languageIds.python]: '#',
  [languageIds.golang]: '//',
  [languageIds.c]: '//',
};

const commentLine = (lineString, languageId) =>
  `${commentLineMapping[languageId]} ${lineString}`;

module.exports = {
  commentLine,
};
