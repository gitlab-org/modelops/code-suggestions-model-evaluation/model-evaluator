const fs = require('fs');
const path = require('path');
const { v4: uuidv4 } = require('uuid');
const {
  programmingLanguages,
  languageModels,
  isSupportedProgrammingLanguage,
} = require('../../constants');
const { commentLine } = require('./language-utils');

const completionFolderName = 'completion';
const generationFolderName = 'generation';
const languageIds = programmingLanguages.map((language) => language.id);

class ArgumentError extends Error {
  constructor(argumentName, allowedValues) {
    const allowedValuesString = allowedValues
      .map((value) => `"${value.id}"`)
      .join(', ');

    super(
      `Wrong argument: ${argumentName}. It should be one of the following strings: ${allowedValuesString} `
    );
  }
}

class PromptFinder {
  #promptVersion = '';
  #baseDataPath = '';
  #results;

  constructor(baseDataPath, version) {
    this.#promptVersion = version;
    this.#baseDataPath = baseDataPath;
    this.#results = {};

    this.#init();
  }

  getPromptsForLanguage(languageId) {
    return this.#results[languageId];
  }

  #init() {
    for (const languageId of languageIds) {
      const completions = this.#getPromptDataFromFiles(
        languageId,
        completionFolderName
      );
      const generations = this.#getPromptDataFromFiles(
        languageId,
        generationFolderName
      );

      const processedGenerations = this.#postprocessGenerations(
        generations,
        languageId
      );
      this.#results[languageId] = [...completions, ...processedGenerations];
    }
  }

  #postprocessGenerations = (generations, languageId) => {
    if (this.#promptVersion === 'v2') {
      return generations.map((promptData) => ({
        ...promptData,
        prompt: commentLine(promptData.prompt, languageId),
      }));
    }

    return generations;
  };

  #getFolderForLanguage = (languageId, promptType) => {
    if (!isSupportedProgrammingLanguage(languageId)) {
      throw new ArgumentError('language', programmingLanguages);
    }

    if (
      promptType !== completionFolderName &&
      promptType !== generationFolderName
    ) {
      throw new ArgumentError('promptType', [
        completionFolderName,
        generationFolderName,
      ]);
    }

    return path.resolve(
      path.join(this.#baseDataPath, this.#promptVersion, promptType, languageId)
    );
  };

  #getPromptDataFromFiles(languageId, promptType) {
    const folderPath = this.#getFolderForLanguage(languageId, promptType);
    if (!fs.existsSync(folderPath)) {
      return [];
    }
    const promptData = fs.readdirSync(folderPath).map((filePath) => {
      const id = path.parse(filePath).name;
      const fileContent = JSON.parse(
        fs.readFileSync(path.join(folderPath, filePath))
      );
      return {
        id,
        ...fileContent,
      };
    });

    const enrichedData = promptData
      .map((data) => {
        return languageModels
          .filter((modelId) => {
            return data[modelId]?.result;
          })
          .map((modelId) => ({
            id: `${languageId}-${modelId}-${promptType}-${
              this.#promptVersion
            }-${data.id}`,
            model: modelId || null,
            language: data.language,
            promptResultId: data[modelId].uuid || uuidv4(), // TODO: remove fallback
            prompt: data.prompt,
            generatedCode: data[modelId].result,
          }));
      })
      .flat();

    return enrichedData;
  }
}

module.exports = PromptFinder;
