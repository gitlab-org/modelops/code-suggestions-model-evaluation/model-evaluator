const { env } = require('process');
const session = require('express-session');
const passport = require('passport');
const LocalStrategy = require('passport-local');
const iu = require('./is-internal-user');
const { authRoutes, authenticated } = require('./auth-routes');

const useAuth = (app) => {
  let secure = false;

  if (env.NODE_ENV === 'production') {
    app.set('trust proxy', 1);
    secure = true;
  }

  app.use(
    session({
      secret: 'keyboard cat',
      resave: true,
      saveUninitialized: true,
      cookie: { secure }, // needed for http without the s in dev
    })
  );

  passport.serializeUser(function (user, done) {
    done(null, user);
  });

  passport.deserializeUser(function (user, done) {
    done(null, user);
  });

  passport.use(
    new LocalStrategy(async function verify(username, password, cb) {
      const internal = await iu.isInternalUser(username, password);
      if (!internal) {
        return cb(null, false, { message: 'Incorrect username or password.' });
      }

      return cb(null, { authenticated_user: { username } });
    })
  );

  app.use(passport.initialize());
  app.use(passport.session());
};

module.exports = {
  useAuth,
  authRoutes,
  authenticated,
};
