const { Router } = require('express');
const passport = require('passport');
const iu = require('./is-internal-user');

const router = Router();

router.post(
  '/auth/login',
  passport.authenticate('local', {
    successRedirect: '/rate-prompts',
    failureRedirect: '/login',
  })
);

router.get('/auth/user', authenticated, (req, res) => {
  const user = iu.internalUsers[0];

  res.status(200).send({ user: { username: user.username } });
});

function authenticated(req, res, next) {
  if (!req.isAuthenticated()) {
    return res.status(401).json({ message: 'Not logged in' });
  }
  next();
}

module.exports = { authRoutes: router, authenticated };
