const crypto = require('crypto');
const { env } = require('process');

const internalUsers = [
  {
    id: 1,
    username: env.MODEL_EV_USERNAME,
    hashed_password: env.MODEL_EV_HASHED_PASSWORD,
  },
];

const salt = env.MODEL_EV_SALT;

async function isInternalUser(username, password) {
  const pwd = await correctPassword(password);
  return internalUsers[0].username === username && pwd;
}

async function correctPassword(submittedPassword) {
  const submittedHashedPassword = await hashPassword(submittedPassword);

  return crypto.timingSafeEqual(
    Buffer.from(submittedHashedPassword),
    Buffer.from(env.MODEL_EV_HASHED_PASSWORD)
  );
}

function hashPassword(password) {
  return new Promise((resolve, reject) => {
    const iterations = 210000;
    const keylen = 64;
    const digest = 'sha512';

    crypto.pbkdf2(password, salt, iterations, keylen, digest, (err, key) => {
      if (err) {
        reject(err);
      } else {
        resolve(key.toString('hex'));
      }
    });
  });
}

module.exports = {
  isInternalUser,
  hashPassword,
  internalUsers,
};
