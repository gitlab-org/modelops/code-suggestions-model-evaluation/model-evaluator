# Model Evaluations Viewer Application

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

### Setup Authentication ENV variables

You can hash the password and put it in MODEL_EV_HASHED_PASSWORD in .env.

The username and assword is stored in 1password here:

https://share.1password.com/s#28rOQoz-n866DbQj3zru2UvYjkjooN1tQwndaBxhEvo

To hash the password:

```
node
const iu = require('./api/is-internal-user');
await iu.hashPassword(<1password-pass>)
```

# Run the tests with a database service via docker compose

From the `model-evaluator` directory:

```
docker-compose build
docker-compose up -d
docker compose exec webapp yarn test
```

# Local Database setup

To get voting working you will need a local database running.

Ensure postgres is installed locally and or you've started gcloud auth proxy

Copy .env.example to .env and fill out your local database credentials. Source those credentials into the shell:

```
source .env
```

If running a local postgres setup create the database.

Otherwise create an access request to get access to
the development cloud sql database.

Ensure knex is installed globally:

```
npm install knex -g
```

Migrate the database:

```
knex migrate:latest --env development
```

### Gcloud dev database

Download gcloud proxy https://cloud.google.com/sql/docs/postgres/connect-auth-proxy#install

Install gcloud https://cloud.google.com/sdk/docs/install

## Special Directories

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).

### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).
