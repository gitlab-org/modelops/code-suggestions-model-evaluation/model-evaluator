const jestConfig = {
  moduleNameMapper: {
    '__app__/(.*)$': '<rootDir>/$1',
  },
};

module.exports = jestConfig;
