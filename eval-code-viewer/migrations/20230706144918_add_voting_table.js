exports.up = function (db) {
  const upQuery = `
    CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

    CREATE TABLE votes(
      id SERIAL PRIMARY KEY,
      prompt_result_id UUID NOT NULL,
      model VARCHAR(255) NOT NULL,
      option VARCHAR(255) NOT NULL,
      language VARCHAR(255) NOT NULL,
      session_id UUID
    );
  `;
  return db
    .raw(upQuery)
    .then(() => {
      console.log('Table "votes" create successfully.');
    })
    .catch((error) => {
      console.error('An error occurred while creating the table:', error);
    });
};

exports.down = function (db) {
  return db.schema.dropTable('votes');
};
