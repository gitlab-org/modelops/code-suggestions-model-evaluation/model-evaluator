#!/bin/sh

if [ ! -z "$DB_INSTANCE_CONNECTION_NAME" ]; then
    wget -O cloud-sql-proxy https://storage.googleapis.com/cloud-sql-connectors/cloud-sql-proxy/v2.5.0/cloud-sql-proxy.linux.amd64
    chmod +x cloud-sql-proxy

    ./cloud-sql-proxy $DB_INSTANCE_CONNECTION_NAME --auto-iam-authn &
fi

yarn start
