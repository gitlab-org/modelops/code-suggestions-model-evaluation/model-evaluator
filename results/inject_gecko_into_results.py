import json

languages = ["python", "golang", "c", "javascript"]
modes = ["generation", "completion"]

for mode in modes:

    for language in languages:

        prompt_num = 0

        for i in range(25):
            prompt_num += 1
            
            with open(f"results/v2/{mode}/{language}/{prompt_num}.json", "r") as f:
                collected_data_json_object = json.load(f)
            
            with open(f"results/ERROR/{mode}/{language}/{prompt_num}.json", "r") as f:
                vertex_gecko_model_response = json.load(f)
            
            vertex_gecko_generated_data = collected_data_json_object["vertex_gecko"]

            new_vertex_gecko_data = {
                    "result": vertex_gecko_model_response["vertex_gecko"]["result"],
                    "duration": vertex_gecko_model_response["vertex_gecko"]["duration"],
                    "result_length": vertex_gecko_model_response["vertex_gecko"]["result_length"],
                    "average_duration": vertex_gecko_model_response["vertex_gecko"]["average_duration"],
                    "raw_durations": vertex_gecko_model_response["vertex_gecko"]["raw_durations"],
                    "uuid": vertex_gecko_generated_data["uuid"]
            }

            collected_data_json_object["vertex_gecko"] = new_vertex_gecko_data

            j_string = json.dumps(collected_data_json_object, indent=2)

            with open(f"results/v2/{mode}/{language}/{prompt_num}.json", "w") as f:
                f.write(j_string)