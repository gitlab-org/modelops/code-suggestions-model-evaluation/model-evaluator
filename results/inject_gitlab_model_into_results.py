import json
import uuid

languages = ["python", "golang", "c", "javascript"]
modes = ["generation", "completion"]

for mode in modes:

    for language in languages:

        prompt_num = 0

        for i in range(25):
            prompt_num += 1
            
            with open(f"results/v2/{mode}/{language}/{prompt_num}.json", "r") as f:
                collected_data_json_object = json.load(f)
            
            with open(f"results/v1/{mode}/{language}/{prompt_num}.json", "r") as f:
                gitlab_model_response_json = json.load(f)

            try:
                gitlab_model_data = {
                        "result": gitlab_model_response_json["gitlab_model"]["result"],
                        "duration": gitlab_model_response_json["gitlab_model"]["duration"],
                        "result_length": gitlab_model_response_json["gitlab_model"]["result_length"],
                        "average_duration": gitlab_model_response_json["gitlab_model"]["avg_duration"],
                        "uuid": str(uuid.uuid4())
                }
            except:
                gitlab_model_data = {"result": ""}

            collected_data_json_object["gitlab_model"] = gitlab_model_data

            j_string = json.dumps(collected_data_json_object, indent=2)

            with open(f"results/v2/{mode}/{language}/{prompt_num}.json", "w") as f:
                f.write(j_string)