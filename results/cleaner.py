import re

import json

languages = ["python", "golang", "c", "javascript"]
modes = ["generation", "completion"]
models = ["anthropic", "anthropic_fast", "vertex_combined", "vertex_code_bison", "vertex_text_bison", "vertex_gecko", "gitlab_model"]

def trim_by_regex(str: str, pat: str = r"```([\s\S]*?)\n"):
    
    str = str.lstrip('\n')
    
    str = re.sub(pat, "", str)

    str = str.lstrip('\n')

    str = str.lstrip()

    return str

for mode in modes:

    for language in languages:

        prompt_num = 0

        for i in range(25):
            prompt_num += 1
            
            with open(f"results/v2/{mode}/{language}/{prompt_num}.json", "r") as f:
                collected_data_json_object = json.load(f)

            for model in models:

                try:
                    unclean_prompt = collected_data_json_object[f"{model}"]["result"]
                    old_length = collected_data_json_object[f"{model}"]["result_length"]

                    cleaned_prompt = trim_by_regex(unclean_prompt)

                    collected_data_json_object[f"{model}"]["result"] = cleaned_prompt
                    collected_data_json_object[f"{model}"]["result_length"] = len(cleaned_prompt)

                except:
                    continue
            
            j_string = json.dumps(collected_data_json_object, indent=2)

            with open(f"results/v3/{mode}/{language}/{prompt_num}.json", "w") as f:
                f.write(j_string)