from chatanthropicnew import ChatAnthropicNew
from codeutils import get_code_from_md_block
from langchain.schema import AIMessage, HumanMessage

token_target_creation = 256
token_target_completion = 128

llm_anthropic = ChatAnthropicNew(model="claude-v1", temperature="0.1", max_tokens_to_sample=token_target_completion)
llm_anthropic_fast = ChatAnthropicNew(
    model="claude-instant-v1",
    temperature="0.1",
    max_tokens_to_sample=token_target_completion,
)


def complete_anthropic_code(language, code, use_fast=False):
    code_parts = code.split("[[CURSOR]]")
    prefix = code_parts[0]
    suffix = code_parts[1] if len(code_parts) > 1 else ""

    system_template = f"""You are GitLab Duo, an AI for writing and completing code created by GitLab. 
    You only respond with Code in a single markdown code block to any question.
    All answers must be valid {language} programs."""

    human_template = f"""Complete the following {language} file. Respond only with code and nothing else:

    ```
    {prefix}
    ```"""

    assistant_template = f"""```{language}"""

    messages = [
        HumanMessage(content=system_template),
        AIMessage(content="Ok"),
    ]

    if suffix:
        suffix_template = f"""Add the following code snippet to your knowledge base:
                    ```{language}
                    {suffix}
                    ```"""

        messages.extend([HumanMessage(content=suffix_template), AIMessage(content="Ok")])

    messages.extend([HumanMessage(content=human_template), AIMessage(content="Ok")])

    if use_fast == True:
        resp = llm_anthropic_fast(messages)
    else:
        resp = llm_anthropic(messages)

    code = get_code_from_md_block(resp.content)
    code = code.replace("```", "")

    return code
