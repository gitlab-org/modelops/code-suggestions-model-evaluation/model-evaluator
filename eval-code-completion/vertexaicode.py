from codeutils import get_code_from_md_block
from google.cloud import aiplatform
from google.protobuf import json_format
from google.protobuf.struct_pb2 import Value

token_target_creation = 256
token_target_completion = 128

google_project = "ai-enablement-dev-69497ba7"


def vertexai_predict(
    model: str,
    input: str,
    parameters: str,
    location: str = "us-central1",
):
    api_endpoint = "us-central1-aiplatform.googleapis.com"
    endpoint = "projects/" + google_project + "/locations/us-central1/publishers/google/models/" + model
    # The AI Platform services require regional API endpoints.
    client_options = {"api_endpoint": api_endpoint}
    # Initialize client that will be used to create and send requests.
    # This client only needs to be created once, and can be reused for multiple requests.
    client = aiplatform.gapic.PredictionServiceClient(client_options=client_options)
    instance_dict = input
    instance = json_format.ParseDict(instance_dict, Value())
    instances = [instance]
    parameters_dict = parameters
    parameters = json_format.ParseDict(parameters_dict, Value())
    response = client.predict(endpoint=endpoint, instances=instances, parameters=parameters)
    print("response ")
    predictions = response.predictions
    result_predictions = []
    for prediction in predictions:
        print(" prediction:", dict(prediction))
        result_predictions.append(dict(prediction))
    return result_predictions


def complete_vertex_text_bison(language, code):
    code_parts = code.split("[[CURSOR]]")
    prefix = code_parts[0]
    suffix = code_parts[1] if len(code_parts) > 1 else ""

    prompt = f"""Do not explain the code or write comments.

    Complete the following {language} code. Respond only with code and nothing else:

    {prefix}
    """

    predictions = vertexai_predict(
        "text-bison@001",
        {"content": prompt},
        {"temperature": 0.1, "maxOutputTokens": token_target_creation},
        "us-central1",
    )

    if predictions:
        prediction_text = predictions[0]["content"]
        print(prediction_text)
        return prediction_text
    else:
        print(predictions)
        return "ERROR"


def complete_vertex_code_bison(language, prefix):
    predictions = vertexai_predict(
        "code-bison@001",
        {
            "prefix": prefix,
        },
        {"temperature": 0.1, "maxOutputTokens": token_target_creation},
        "us-central1",
    )

    if predictions:
        prediction_text = predictions[0]["content"]
        code = get_code_from_md_block(prediction_text)
        return code
    else:
        return "ERROR"


def complete_vertex_code_gecko(prefix, suffix):
    GECKO_MAXIMUM_TOKEN_OUTPUT = 64

    predictions = vertexai_predict(
        "code-gecko@001",
        {"prefix": prefix, "suffix": suffix},
        {"temperature": 0.2, "maxOutputTokens": GECKO_MAXIMUM_TOKEN_OUTPUT},
        "us-central1",
    )

    if predictions:
        prediction_text = predictions[0]["content"]
        return prediction_text
    else:
        return "ERROR"


def complete_vertex_gecko(language, code):
    code_parts = code.split("[[CURSOR]]")
    prefix = code_parts[0]
    suffix = code_parts[1] if len(code_parts) > 1 else ""

    return complete_vertex_code_gecko(prefix, suffix)


def complete_vertex_code(language, code):
    code_parts = code.split("[[CURSOR]]")
    prefix = code_parts[0]
    suffix = code_parts[1] if len(code_parts) > 1 else ""

    print("Pre : " + prefix)
    print("Suff : " + suffix)

    if suffix:
        print("USE GECKO AS WE HAVE SUFFIX")
        res = complete_vertex_code_gecko(prefix, suffix)
    else:
        if len(prefix.split("\n")) < 5:
            print("USE CODE BISON AS LENGTH IS SMALLER THEN 5 SO MOST PROBABLY CODE GENERATION")
            # This can be proper checking if its a comment that was just written for example
            res = complete_vertex_code_bison(language, prefix)
        else:
            print("USE GECKO")
            res = complete_vertex_code_gecko(prefix, suffix)

    return res
