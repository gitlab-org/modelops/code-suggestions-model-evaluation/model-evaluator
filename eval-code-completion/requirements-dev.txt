black==23.7.0
flake8==6.0.0
isort==5.12.0
autoflake==2.2.0