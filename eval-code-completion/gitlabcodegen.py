import os

import requests

gitlab_token = os.environ["GITLAB_TOKEN"]


def complete_gitlab_native_code(language, code):
    gitlab_headers = {
        "Authorization": "Bearer " + gitlab_token,
        "Content-Type": "application/json",
    }

    code_parts = code.split("[[CURSOR]]")
    prefix = code_parts[0]
    suffix = code_parts[1] if len(code_parts) > 1 else ""

    print("Pre : " + prefix)
    print("Suff : " + suffix)

    filename = ""
    if language == "python":
        filename = "test.py"
    elif language == "javascript":
        filename = "test.js"
    elif language == "ruby":
        filename = "test.rb"
    elif language == "golang":
        filename = "test.go"

    gl_json_data = {
        "prompt_version": 1,
        "project_path": "gitlab-org/gitlab-shell",
        "project_id": 33191677,
        "current_file": {
            "file_name": filename,
            "content_above_cursor": prefix,
            "content_below_cursor": suffix,
        },
    }

    response = requests.post(
        "https://codesuggestions.gitlab.com/v2/completions",
        headers=gitlab_headers,
        json=gl_json_data,
    )
    prediction_content = response.json()
    print(prediction_content)

    if prediction_content.get("choices"):
        return prediction_content["choices"][0]["text"]
    else:
        return "ERROR"
