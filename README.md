# Langchain Tests

## Using Model Evaluation Testing Script

### What does this do?

The model evaluation testing script runs testing on a total of 7 different machine learning models. The models include: anthropic, anthropic_fast, vertex_combined, vertex_code_bison, vertex_text_bison, vertex_gecko, gitlab_model. As of the time this is written, the `gitlab_model` is no longer up and running and most likely won't be included in the future iterations of manual evaluations. The testing is done on four different languages: Python, Javascript, C, Golang. An overview of the script is that it takes a `.txt` file as an input where each line is a prompt. The script ingests the prompts, languages (hard coded), and the models (hard coded) and produces `.txt` file output as a JSON file. There is one JSON file for each prompt/language combination. The metrics included in the JSON blocks are:

- `result` - the output from the model
- `duration` - the time (in seconds) from when the model is called to when the response is collected
- `length` - the length of the result from the model measured by number of characters in the `result` string
- `average_duration` - an average of the `duration`'s for 10 prompt/langauge runs
- `raw_durations` - the raw `durations`'s collected to calculate `average_duration`
- `uuid` - a unique identifier to keep track of each prompt/langauge combination

### Pre-requisites
The best way to run the script is by creating a virtual environment in Python and use the `requirements.txt` file to ensure all dependencies are accessible. The steps to accomplish that are as follows:

If `virtualenv` is not already installed, use `pip` to do so,
```shell
pip install virtualenv
```

Confirm that `virtualenv` is downlaoded,
```shell
> virtualenv --version
virtualenv 20.17.0
```

Use `virtualenv` to create a virtual environemnt:
```shell
python3.11 -m virtualenv venv
```

A "(venv)" indicator should pop up in the directory of the project and, if activated, should be indicated in the CLI.

If at any point the virtual environment becomes deactivated (the "venv" indicator is no longer in the CLI), run this command:
```shell
source ./venv/bin/activate
```

If at any point the virtual environment needs to be toggled off, run this command:
```shell
deactivate
```

Once the virtual environment is configured, run the following command to get the required dependencies:
```shell
pip install -r eval-code-completion/requirements.txt
```


### Using make targets
The script for model evaluation is split in to four different make targets. Running

```shell
make run-generation
```
is going to run the model evaluation script with the generation scripts. This testing prompts the models to return some sort of code with plain english as the input. The prompts for the generation testing can be found [here](https://gitlab.com/gitlab-org/modelops/code-suggestions-model-evaluation/model-evaluator/-/tree/main/results/v1/prompts/generation).

On the contrary, running

```shell
make run-completion
```

is going to run the model evaluation script with the completion scripts. This testing prompts the models to return code that would best fit the code given as the input. Note that these prompts are currently (as of 26/06/2023) the same as those used for generation, but translated into code. Most of the prompts consist of the headings for functions, simple declarations for classes, or initializations of dictionaries. The prompts for the completion testing can be found [here](https://gitlab.com/gitlab-org/modelops/code-suggestions-model-evaluation/model-evaluator/-/tree/main/results/v1/prompts/completion).

The last two make targets are specific to the `vertex_gecko` model. This model does not return results under the regular collcetion harness and as a result needs to be called separately. In order for these make targets to work correctly, there are some manual steps required before using them.

First, create a new results folder that indicates a new version of testing results (for example `gecko`) in the `/results` directory. Within the `/results/gecko` directory, there should be sub directories: `results/gecko/generation` and `results/gecko/completion`. And finally within each of those sub-directories, there should be an empty folder for each language that data is being collected for: golang, c, javascript, python. The directory should look something like this:

```shell
└── v3
    ├── completion
    │   ├── c
    │   ├── golang
    │   ├── javascript
    │   └── python
    └── generation
        ├── c
        ├── golang
        ├── javascript
        └── python
```

Now that the file structure for collecting `vertex_gecko` data is set up some last few changes need to be made. In `results/evaluation_vertex_gecko.py`, find the functions with the title `write_response_generation` and `write_response_completion`. In these functions change the `ERROR` directory in the file path to whatever the filepath has been created above for collecting `vertex_gecko` data. In this example, `ERROR` would be replaced with `vN` where N is whatever version of data being collected.

Now to get `vertex_gecko` to return results from the model, open two different terminals and run the following two commands in each of the terminals:

```shell
make run-vertex-gecko-generation
```

```shell
make run-vertex-gecko-completion
```

This will take about 2 1/2 hours total. Once that process is complete, run the following make target to inject the data that was just collected for `vertex_gecko` into whatever `/results/vN` file path is neccessary.

```shell
make inject-vertex-gecko
```

If all goes well, the data in the `/results/gecko` can be deleted and the `results/vN` data should be updated.